class EmployeesController < ApplicationController
	include ParamsHelper

	def new
		@employee = Employee.new
		@need_work = true
	end

	def vacancies
		@employee = Employee.find(params[:id])
		@exact_match_vacancies = @employee.get_exact_match_vacancies
		@fuzzy_match_vacancies = @employee.get_fuzzy_match_vacancies(@exact_match_vacancies)
	end

	def index
		@employees = Employee.paginate(page: params[:page])
	end

	def edit
		@employee = Employee.find(params[:id])
		@existing_skill_ids = @employee.skills.map { |skill| skill.id.to_s }
		@need_work = @employee.looking_for_work

		render 'new'
	end

	def update
		@employee = Employee.find(params[:id])
		if @employee.update_attributes(params[:employee])
			flash.now[:success] = "Employee successfully updated!"
		end
		@employee.delete_all_skills
		@employee.add_skills(get_new_skill_names, get_existing_skill_ids)

		# WTF: @employee.skills returns empty array, although we just added skills
		# Because of it we have to use Employee#skill_relationships to get skills IDs
		@existing_skill_ids = @employee.skill_relationships.map { |relationship| relationship.skill_id.to_s }
		@need_work = params[:employee][:looking_for_work] == "1"

		render 'new'
	end

	def create
		@employee = Employee.new(params[:employee])
		if @employee.save
			flash.now[:success] = "New Employee #{params[:employee][:name]} Created"
			@employee.add_skills(get_new_skill_names, get_existing_skill_ids)
			reset_object_state
		else
			@new_skill_names = get_new_skill_names
			@existing_skill_ids = get_existing_skill_ids
			@need_work = params[:employee][:looking_for_work] == "1"
		end
		render 'new'
	end

	def create_new_skill
		respond_to do |format|
       		format.js { render 'shared/create_new_skill' }
    	end
	end

private
	def reset_object_state
		@employee = Employee.new
		@new_skill_names = []
		@existing_skill_ids = []
		@need_work = true
	end
end
