class VacanciesController < ApplicationController
	include ParamsHelper

	def new
		@vacancy = Vacancy.new
	end

	def index
		@vacancies = Vacancy.paginate(page: params[:page])
	end

	def employees
		@vacancy = Vacancy.find(params[:id])
		@exact_match_employees = @vacancy.get_exact_match_employees
		@fuzzy_match_employees = @vacancy.get_fuzzy_match_employees(@exact_match_employees)
	end

	def edit
		@vacancy = Vacancy.find(params[:id])
		@existing_skill_ids = @vacancy.skills.map { |skill| skill.id.to_s }
		render 'new'
	end

	def update
		@vacancy = Vacancy.find(params[:id])
		if @vacancy.update_attributes(params[:vacancy])
			if @vacancy.update_attribute(:elapsed, params[:elapsed])
				flash.now[:success] = "Vacancy successfully updated!"
			end
		end
		@vacancy.delete_all_skills
		@vacancy.add_skills(get_new_skill_names, get_existing_skill_ids)

		# WTF: @vacancy.skills returns empty array, although we just added skills
		# Because of it we have to use Vacancy#skill_relationships to get skills IDs
		@existing_skill_ids = @vacancy.skill_relationships.map { |relationship| relationship.skill_id.to_s }

		render 'new'
	end

	def create
		@vacancy = Vacancy.new(params[:vacancy])
		@vacancy.elapsed = params[:elapsed]
		if @vacancy.save
			flash.now[:success] = "New Vacancy #{params[:vacancy][:name]} Created"
			@vacancy.add_skills(get_new_skill_names, get_existing_skill_ids)
			reset_object_state
		else
			@new_skill_names = get_new_skill_names
			@existing_skill_ids = get_existing_skill_ids
		end
		render 'new'
	end

	def create_new_skill
		respond_to do |format|
       		format.js { render 'shared/create_new_skill'}
    	end
	end

private
	def reset_object_state
		@vacancy = Vacancy.new
		@new_skill_names = []
		@existing_skill_ids = []
	end
end
