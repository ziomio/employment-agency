module DatabaseHelper

## Add/Delete skills
	def delete_all_skills
		self.skills.delete_all
	end

	def add_skills(new_skill_names, existing_skill_ids)
	  	new_skill_names.each do |skill_name|
	  		if existing_skill = Skill.find_by_name(skill_name)
				self.skill_relationships.create(skill_id: existing_skill.id)
			else
				self.skills.create(name: skill_name)
			end
	  	end

		existing_skill_ids.each do |skill_id|
			self.skill_relationships.create(skill_id: skill_id)
		end unless existing_skill_ids.nil?
	end

## Search vacancies/employees in DB
	def get_exact_match_vacancies
		matching_vacancies = []
		Vacancy.all.each do |vacancy|
			intersection = vacancy.skills & self.skills
			if !intersection.empty? && intersection.size == vacancy.skills.size
				matching_vacancies << vacancy.id if !vacancy.expired?
			end
		end
		matching_vacancies
	end

	def get_fuzzy_match_vacancies(excluded_ids)
		matching_vacancies = []
		Vacancy.all.each do |vacancy|
			intersection = vacancy.skills & self.skills
			unless intersection.empty?
				matching_vacancies << vacancy.id if !excluded_ids.include?(vacancy.id) && !vacancy.expired?
			end
		end
		matching_vacancies
	end

	def get_exact_match_employees
		matching_employees = []
		Employee.all.each do |employee|
			intersection = employee.skills & self.skills
			if !intersection.empty? && intersection.size == self.skills.size
				matching_employees << employee.id if employee.looking_for_work
			end
		end
		matching_employees
	end

	def get_fuzzy_match_employees(excluded_ids)
		matching_employees = []
		Employee.all.each do |employee|
			intersection = employee.skills & self.skills
			unless intersection.empty?
				matching_employees << employee.id if !excluded_ids.include?(employee.id) && employee.looking_for_work
			end
		end
		matching_employees
	end

end