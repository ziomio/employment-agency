module EmployeesHelper

	def number_of_elements_in(container)
		if container.nil?
			0
		else
			container.count
		end
	end

	def checked?(skill_id, collection)
		if collection.nil?
			false
		else
			collection.include?(skill_id.to_s)
		end
	end

	def highlight_skill(need_highlight)
		"highlight" if need_highlight
	end

	def record_number_for(index)
		page = params[:page] ? params[:page] : "1"
		WillPaginate.per_page * (page.to_i - 1) + index
	end
end