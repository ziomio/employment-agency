module ParamsHelper
	def get_new_skill_names
		skill_names = []
		params[:new_skills_cnt].to_i.times do |i|
			name = params["new_skill_#{i}"]
			skill_names << name unless name.blank?
		end
		skill_names
	end

	def get_existing_skill_ids
		params[:skills]
	end

	def page_title_for(activeRecordObject)
		if activeRecordObject.nil? || activeRecordObject.new_record?
			"Create #{activeRecordObject.class.name}"
		else
			"Edit #{activeRecordObject.class.name}"
		end
	end

	def back_path_for(activeRecordObject)
		if activeRecordObject.nil? || activeRecordObject.new_record?
			root_path
		else
			# FIXME: eval is evil? Need to generate path from string using another approach
			eval("#{activeRecordObject.class.name.pluralize.downcase}_path")
		end
	end
end