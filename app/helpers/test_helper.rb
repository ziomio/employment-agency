#encoding: utf-8
module TestHelper

	def make_skills
		skills = %w(C++ Java Python Ruby C Objective-C MySQL PostgreSQL NoSQL XML Ajax jQuery
					CSS HTML DOM Delphi Haskell MongoDB Scala)
		skills.each { |skill| Skill.create!(name:skill) }
	end
end