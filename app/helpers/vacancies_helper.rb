module VacanciesHelper
	def generate_years_list
		(1..10).map do |value|
			["In #{pluralize(value, 'year')}", value]
		end
	end

	def generate_months_list
		(1..12).map do |value|
			["#{pluralize(value, 'month')}", value]
		end
	end

	def generate_days_list
		(1..31).map do |value|
			["#{pluralize(value, 'day')}", value]
		end
	end

	def selected_year(hash)
		hash[:years] unless hash.nil?
	end

	def selected_day(hash)
		hash[:days] unless hash.nil?
	end

	def selected_month(hash)
		if hash.nil? || hash.empty?
			6
		else
			hash[:months]
		end
	end
end
