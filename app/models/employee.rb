#encoding: utf-8
class Employee < ActiveRecord::Base
	include DatabaseHelper

	attr_accessible :contact_info, :desired_salary, :looking_for_work, :name
	has_many :skill_relationships, class_name: "EmployeeSkillRelationship", dependent: :destroy
	has_many :skills, through: :skill_relationships

	before_validation do |employee|
		employee.name = employee.name.gsub(/\s+/, ' ').strip
	end

  validates :desired_salary, presence: true, numericality: { only_integer: true }
  validates :contact_info, presence: true

  # FIXME: if format is wrong - a message "is invalid" appears, instead of specified one
  validates :name, presence: true, format: { with: /^(([\p{Cyrillic}]+)\s){2}\p{Cyrillic}+$/,
  				   			 											 messsage: "Name should consist from 3 cyrillic words" }
end
