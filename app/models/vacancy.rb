class Vacancy < ActiveRecord::Base
  include DatabaseHelper
  serialize :elapsed, Hash
  attr_accessible :added, :contact_info, :elapsed, :name, :salary
  has_many :skill_relationships, class_name: "VacancySkillRelationship", dependent: :destroy
  has_many :skills, through: :skill_relationships

  validates :name, presence: true
  validates :salary, presence: true, numericality: { only_integer: true }
  validates :contact_info, presence: true
  validates :added, presence: true
  validates :elapsed, presence: true

  def expired?
  	Date.today > elapsed_to_date
  end

private
  def elapsed_to_date
	elapsed = self.added
	elapsed += self.elapsed[:days].to_i.days
	elapsed += self.elapsed[:months].to_i.months
	elapsed += self.elapsed[:years].to_i.years
  end
end
