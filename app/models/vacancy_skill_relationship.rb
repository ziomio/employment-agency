class VacancySkillRelationship < ActiveRecord::Base
  attr_accessible :skill_id, :vacancy_id
  belongs_to :vacancy
  belongs_to :skill
end
