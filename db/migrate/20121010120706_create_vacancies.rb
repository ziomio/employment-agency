class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.date :added
      t.date :elapsed
      t.integer :salary
      t.string :contact_info
      t.string :required_skills

      t.timestamps
    end
  end
end
