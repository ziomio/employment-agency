class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :contact_info
      t.boolean :looking_for_work
      t.integer :desired_salary
      t.string :skills

      t.timestamps
    end
  end
end
