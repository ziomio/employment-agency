class CreateVacancySkillRelationships < ActiveRecord::Migration
  def change
    create_table :vacancy_skill_relationships do |t|
      t.integer :skill_id
      t.integer :vacancy_id

      t.timestamps
    end
  end
end
