class RemoveRequiredSkillsFromVacancy < ActiveRecord::Migration
  def up
    remove_column :vacancies, :required_skills
  end

  def down
    add_column :vacancies, :required_skills, :string
  end
end
