class RemoveSkillsFromEmployee < ActiveRecord::Migration
  def up
    remove_column :employees, :skills
  end

  def down
    add_column :employees, :skills, :string
  end
end
