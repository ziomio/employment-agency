class ChangeDataTypeForVacancyElapsed < ActiveRecord::Migration
  def up
  	change_column :vacancies, :elapsed, :text
  end

  def down
  	change_column :vacancies, :elapsed, :date
  end
end
