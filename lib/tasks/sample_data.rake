#encoding: utf-8

require_relative '../../app/helpers/test_helper'

namespace :db do
  include TestHelper
  desc "Fill database with sample data"
  task populate: :environment do
    make_skills
    make_employees
    make_vacancies
  end
end

def generate_employee_name
	first_name = %w(Вася Петя Оля Сережа Тоня Денис Антон Артур Кирилл Степан Виктор Анна Алиса Татьяна)
	last_name = %w(Смирнов Иванов Кузнецов Попов Соколов Лебедев Козлов Новиков Морозов Петров Волков)
	middle_name = %w(Васильевич Петрович Сергеевич Денисович Антонович Артурович Кириллович Степанович)
	"#{last_name.sample} #{first_name.sample} #{middle_name.sample}"
end

def make_employees
	100.times do
		e = Employee.create!(name: generate_employee_name,
				 	 contact_info: "employee@example.com",
			 	 looking_for_work: [true, false].sample,
			   	   desired_salary: 300 + rand(15000))

		Skill.all.each do |skill|
			e.skill_relationships.create(skill_id: skill.id) if [true, false].sample
		end
	end
end

def make_vacancies
	100.times do
		v = Vacancy.create!(name: "#{Faker::Company.name} developer",
						   added: rand(50).days.ago,
				  elapsed:{years: rand(11), months: rand(13), days: rand(32)},
						  salary: 300 + rand(25000),
					contact_info: "vacancy@example.com")

		Skill.all.each do |skill|
			v.skill_relationships.create(skill_id: skill.id) if [true, false].sample
		end
	end
end