#encoding: utf-8

FactoryGirl.define do
	factory :employee do
		name 		    "Сара Мишель Геллар"
		contact_info    "sahra@gmail.com"
		desired_salary   2500
		looking_for_work true
		skills 			 { prepare_skills("Ruby", "HTML", "CSS") }
	end

	factory :vacancy do
		name 		 "Ruby developer"
		salary 		  1500
		contact_info "hr@ruby.com"
		added 		  Date.today
		elapsed 	  Hash["years" => "1", "months" => "2", "days" => "12"]
		skills 		  { prepare_skills("Ruby", "HTML", "CSS") }
	end
end