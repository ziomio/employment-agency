#encoding: utf-8
require 'spec_helper'

describe Employee do
	before do
		@employee = Employee.new(name: "Сара Мишель Геллар", contact_info: "sahra@gmail.com",
								 desired_salary: 1000, looking_for_work: true)
	end
	after { Employee.delete_all }

	subject { @employee }
	it { should be_valid }
	it { should respond_to(:skills) }
	it { should respond_to(:skill_relationships) }

	describe "creating skill" do
		before { @employee.save }

		it "should create a record in Skill table" do
			expect { @employee.skills.create(name: "Ruby") }.to change(Skill, :count).by(1)
		end

		it "should create a record in relationship table" do
			expect { @employee.skills.create(name: "Ruby") }.to change(EmployeeSkillRelationship, :count).by(1)
		end
	end

	describe "creating a new relationship" do
		before do
			@employee.save
		 	@skill = Skill.create(name: "Ruby")
		 end
		it "should create a new record in relationship table" do
			expect { @employee.skill_relationships.create(skill_id: @skill.id) }.to change(
																EmployeeSkillRelationship, :count).by(1)
		end
	end

	describe "name" do
		describe "useful spaces" do
			before do
				@employee.name = "  Сара    Мишель    Геллар    "
			end
			it "should be deleted" do
				expect { @employee.save }.to change(@employee, :name).to("Сара Мишель Геллар")
			end
		end

		describe "should be specified and have correct format" do
			describe "wrong format" do
				before do
					@employee.name = "Jeorge Bush"
					@employee.save
				end
				it { should_not be_valid }
			end

			describe "correct format" do
				before do
					@employee.save
				end
				it { should be_valid }
			end
		end
	end

	describe "contact info should be specified" do
		before do
			@employee.contact_info = nil
			@employee.save
		end
		it { should_not be_valid }
	end

	describe "salary" do
		describe "should be specified" do
			before do
				@employee.desired_salary = nil
				@employee.save
			end
			it { should_not be_valid }
		end

		describe "should contain only integers" do
			before do
				@employee.desired_salary = "abcd"
				@employee.save
			end
			it { should_not be_valid }
		end
	end


end
