require 'spec_helper'

describe Skill do
	before do
		@skill = Skill.new(name: "Ruby")
	end

	subject { @skill }
	it { should be_valid }

	describe "with empty name should not be valid" do
		before do
			@skill.name = ""
			@skill.save
		end
		it { should_not be_valid }
	end

	describe "should be unique" do
		before do
			@skill.save
			@second_skill = Skill.create(name: @skill.name)
		end
		specify { @second_skill.should_not be_valid }
	end
end
