require 'spec_helper'

describe Vacancy do
	before do
		@vacancy = Vacancy.new(name: "Ruby developer", salary: 2500, contact_info: "hr@ruby.com",
							   added: Date.today, elapsed: Hash["years" => "2",
							   									"months" => "8",
							   									"days" => "16"])
	end
	after { Vacancy.delete_all }

	subject { @vacancy }

	it { should be_valid }
	it { should respond_to(:skills) }
	it { should respond_to(:skill_relationships) }

	describe "elapsed should be serialized" do
		before { @vacancy.save }
		let(:elapsed) { @vacancy.reload.elapsed }
		specify { elapsed.should eq Hash["years" => "2", "months" => "8", "days" => "16"] }
	end

	describe "creating skill" do
		before { @vacancy.save }

		it "should create a record in Skill table" do
			expect { @vacancy.skills.create(name: "Ruby") }.to change(Skill, :count).by(1)
		end

		it "should create a record in relationship table" do
			expect { @vacancy.skills.create(name: "Ruby") }.to change(VacancySkillRelationship, :count).by(1)
		end
	end

	describe "creating a new relationship" do
		before do
			@vacancy.save
		 	@skill = Skill.create(name: "Ruby")
		 end
		it "should create a new record in relationship table" do
			expect { @vacancy.skill_relationships.create(skill_id: @skill.id) }.to change(
																VacancySkillRelationship, :count).by(1)
		end
	end

	describe "elapsed should be specified" do
		before do
			@vacancy.elapsed = nil
			@vacancy.save
		end
		it { should_not be_valid }
	end

	describe "added should be specified" do
		before do
			@vacancy.added = nil
			@vacancy.save
		end
		it { should_not be_valid }
	end

	describe "name should be specified" do
		before do
			@vacancy.name = ""
			@vacancy.save
		end
		it { should_not be_valid }
	end

	describe "contact info should be specified" do
		before do
			@vacancy.contact_info = nil
			@vacancy.save
		end
		it { should_not be_valid }
	end

	describe "salary" do
		describe "should be specified" do
			before do
				@vacancy.salary = nil
				@vacancy.save
			end
			it { should_not be_valid }
		end

		describe "should contain only integers" do
			before do
				@vacancy.salary = "abcd"
				@vacancy.save
			end
			it { should_not be_valid }
		end
	end
end














