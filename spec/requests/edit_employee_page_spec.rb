#encoding: utf-8
require 'spec_helper'

describe "Edit Employee" do
	include TestHelper

	before do
		make_skills
		FactoryGirl.create(:employee)
	end

	after { Skill.delete_all }

	subject { page }

	describe "changing employee's attributes to new (wrong) ones" do
		#FIXME: DRY violation for :employee
		let(:employee) { Employee.last }
		before do
			visit edit_employee_path(employee.id)
			fill_in "Name", with: "Jeorge Clinton Jr"
			fill_in "Desired salary", with: "d10000"
			fill_in "Contact info", with: ""
			click_button "Update Employee"
		end

		describe "should generate an error messages" do
			it { should have_content "3 errors" }
			it { should have_content "Desired salary is not a number" }
			it { should have_content "Contact info can't be blank" }
			it { should have_content "Name is invalid" }
		end
	end

	describe "changing employee's attributes to new (correct) ones" do
		let(:employee) { Employee.last }
		before do
			visit edit_employee_path(employee.id)
			fill_in "Name", with: "Малинина Светлана Викторовна"
			fill_in "Desired salary", with: 10000
			fill_in "Contact info", with: "sveta@gmail.com"
			uncheck "Looking for work"
		end

		describe "should update its attributes" do
			before { click_button "Update Employee" }
			specify { employee.reload.name.should eq "Малинина Светлана Викторовна" }
			specify { employee.reload.desired_salary.should eq 10000 }
			specify { employee.reload.contact_info.should eq "sveta@gmail.com" }
			specify { employee.reload.looking_for_work.should eq false }
			it { should have_unchecked_field("Looking for work") }
		end

		describe "after selecting new skills" do
			let(:new_skills) { Skill.all.sample(5) }
			let(:old_skills) { employee.skills.map { |skill| skill.id } }
			before do
				# unset old skills
				old_skills.each do |skill_id|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill_id}\"]").set(false)
				end
				# and set new ones
				new_skills.each do |skill|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill.id}\"]").set(true)
				end
			end

			it "should have 5 skills" do
				expect {  click_button "Update Employee" }.to change(employee.skills, :count).to(5)
			end

			describe "skills in DB should have new IDs" do
				let(:db_ids) { employee.reload.skills.map { |skill| skill.id}.sort }
				let(:checked_ids) { new_skills.map { |skill| skill.id }.sort }
				before { click_button "Update Employee" }

				specify { db_ids.should eq checked_ids }
			end
		end
	end
end