#encoding: utf-8
require 'spec_helper'

describe "Edit Vacancy" do
	include TestHelper

	before do
		make_skills
	 	FactoryGirl.create(:vacancy)
	end

	after { Skill.delete_all }

	subject { page }

	describe "changing vacancy's attributes to new (wrong) ones" do
		#FIXME: DRY violation for :vacancy
		let(:vacancy) { Vacancy.last }
		before do
			visit edit_vacancy_path(vacancy.id)
			fill_in "Name", with: ""
			fill_in "Salary", with: "d10000"
			fill_in "Contact info", with: ""
			click_button "Update Vacancy"
		end

		describe "should generate an error messages" do
			it { should have_content "3 errors" }
			it { should have_content "Salary is not a number" }
			it { should have_content "Contact info can't be blank" }
			it { should have_content "Name can't be blank" }
		end
	end

	describe "changing vacancy's attributes to new (correct) ones" do
		let(:vacancy) { Vacancy.last }
		before do
			visit edit_vacancy_path(vacancy.id)
			fill_in "Name", with: "Ruby Senior Developer"
			fill_in "Salary", with: 3500
			fill_in "Contact info", with: "hr.seniors@ruby.com"

			page.select "12", from: "vacancy[added(3i)]"
			page.select "April", from: "vacancy[added(2i)]"
			page.select "2015", from: "vacancy[added(1i)]"

			page.select "In 2 years", from: "elapsed[years]"
			page.select "3 months", from: "elapsed[months]"
			page.select "26 days", from: "elapsed[days]"
		end

		describe "should update its attributes" do
			before { click_button "Update Vacancy" }
			specify { vacancy.reload.name.should eq "Ruby Senior Developer" }
			specify { vacancy.reload.salary.should eq 3500 }
			specify { vacancy.reload.contact_info.should eq "hr.seniors@ruby.com" }
			specify { vacancy.reload.elapsed.should eq Hash["years"  => "2",
														  	"months" => "3",
														  	"days" => "26"] }
			specify { vacancy.reload.added.should eq Date.new(2015, 4, 12) }
		end

		describe "after selecting new skills" do
			let(:new_skills) { Skill.all.sample(5) }
			let(:old_skills) { vacancy.skills.map { |skill| skill.id } }
			before do
				# unset old skills
				old_skills.each do |skill_id|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill_id}\"]").set(false)
				end
				# and set new ones
				new_skills.each do |skill|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill.id}\"]").set(true)
				end
			end

			it "should have 5 skills" do
				expect {  click_button "Update Vacancy" }.to change(vacancy.skills, :count).to(5)
			end

			describe "skills in DB should have new IDs" do
				let(:db_ids) { vacancy.reload.skills.map { |skill| skill.id}.sort }
				let(:checked_ids) { new_skills.map { |skill| skill.id }.sort }
				before { click_button "Update Vacancy" }

				specify { db_ids.should eq checked_ids }
			end
		end
	end
end