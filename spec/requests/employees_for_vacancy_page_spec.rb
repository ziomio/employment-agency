#encoding: utf-8
require 'spec_helper'

describe "Employees For Vacancy" do
	include TestHelper
	include FactoryHelper

	let(:vacancy) { Vacancy.last }
	before do
		make_skills
		FactoryGirl.create(:vacancy, name: "Ruby developer", skills: prepare_skills("Ruby", "HTML", "CSS"))
		FactoryGirl.create(:employee, name: "Сара Мишель Геллар", skills: prepare_skills("Ruby"))
		FactoryGirl.create(:employee, name: "Иван Иванович Иваненко", skills: prepare_skills("Ruby", "HTML"))
		FactoryGirl.create(:employee, name: "Джон Мак Гован", skills: prepare_skills("Ruby", "HTML", "CSS", "XML"))
		visit employees_for_vacancy_path(vacancy)
	end

	subject { page }
	it { should have_selector('title', text: "Employees for vacancy: \"#{vacancy.name}\"") }
	it { should have_selector('h1', text: "Employees for vacancy: \"#{vacancy.name}\"") }

	it { should have_css("table#perfect_match_table tr"), count: 1 }
	it { should have_css("table#fuzzy_match_table tr"), count: 2 }

	it { find('#perfect_match_table').should have_content('Джон Мак Гован') }
	it { find('#perfect_match_table').should have_content('XML') }

	it { find('#fuzzy_match_table').should have_content('Сара Мишель Геллар') }
	it { find('#fuzzy_match_table').should have_content('Иван Иванович Иваненко') }
end















