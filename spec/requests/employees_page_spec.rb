#encoding: utf-8
require 'spec_helper'

describe "Employees list" do
	include TestHelper
	include FactoryHelper

	before do
		make_skills
		FactoryGirl.create(:employee, name: "Марта Стюарт Младшая", skills: prepare_skills("Ruby"))
		FactoryGirl.create(:employee, name: "Джон Мак Гован", skills: prepare_skills("Ruby", "HTML"))
		FactoryGirl.create(:employee, name: "Питер Пен Настоящий", skills: prepare_skills("Ruby", "HTML", "CSS", "XML"))

		WillPaginate.per_page = 2
		visit employees_path
	end

	subject { page }
	it { should have_selector('h1', text: "Employees list") }
	it { should have_selector('title', text: "Employees list") }
	it { should have_selector('a', href: "employees?page=2", content: "Next")  }

end