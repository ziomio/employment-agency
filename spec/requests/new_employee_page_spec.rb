#encoding: utf-8
require 'spec_helper'

describe "Create New Employee" do
	include TestHelper
	before do
		make_skills
	 	visit new_employee_path
	end

	after { Skill.delete_all }

	subject { page }

	it { should have_selector('title', text: 'Create Employee') }
	it { should have_selector('h1', text: 'Create Employee') }
	it { should have_checked_field("Looking for work") }

	it "should have all skills from a DB" do
		Skill.all.each do |skill|
			should have_css("input[type=\"checkbox\"][value=\"#{skill.id}\"]")
		end
	end

	describe "filling in correct info" do
		before do
			fill_in "Name", with: "Кэти Перри Младшая"
			fill_in "Desired salary", with: "2500"
			fill_in "Contact info", with: "cathy@gmail.com"
		end

		it "should create a new employee with active 'Looking for work' status" do
			expect { click_button "Create Employee" }.to change(Employee, :count).by(1)

		end

		describe "adding a new skill via JavaScript", js: true do
			before do
				click_link "Add new"
				find(:css, "input[type=\"text\"][name=\"new_skill_0\"]").set("Prolog")
			end
			it "should create a new skill" do
				expect { click_button "Create Employee" }.to change(Skill, :count).by(1)
			end
		end

		describe "adding already existing skill via JavaScript", js: true do
			before do
				click_link "Add new"
				find(:css, "input[type=\"text\"][name=\"new_skill_0\"]").set("Ruby")
			end
			it "should not create a new skill" do
				expect { click_button "Create Employee" }.not_to change(Skill, :count)
			end
		end

		describe "selecting 3 skills" do
			before do
				skills = Skill.all.sample(3)
				skills.each do |skill|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill.id}\"]").set(true)
				end
			end

			# WTF: this test failed 1 time (from 200 launches) with message:
			# "count should have been changed by 3, but was changed by 2"
			# Similar test exists in new_vacancy_page_spec.rb - it can be potentially broken also
			it "should create 3 relationships" do
				expect { click_button "Create Employee" }.to change(EmployeeSkillRelationship, :count).by(3)
			end
		end
	end

	describe "filling in wrong info" do
		before do
			fill_in "Name", with: "John Mc Gowan"
			fill_in "Desired salary", with: "dd12dd"
		end

		it "should not create a new employee" do
			expect { click_button "Create Employee" }.not_to change(Employee, :count).by(1)
		end

		describe "should generate error messages" do
			before { click_button "Create Employee" }

			it { should have_content "3 errors" }
			it { should have_content "Desired salary is not a number" }
			it { should have_content "Contact info can't be blank" }
			it { should have_content "Name is invalid" }
		end
	end
end

