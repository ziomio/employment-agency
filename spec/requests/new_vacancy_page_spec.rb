#encoding: utf-8
require 'spec_helper'

describe "Create New Vacancy" do
	include TestHelper
	before do
		make_skills
	 	visit new_vacancy_path
	end

	after { Skill.delete_all }

	subject { page }

	it { should have_selector('title', text: 'Create Vacancy') }
	it { should have_selector('h1', text: 'Create Vacancy') }


	it "should have all skills from a DB" do
		Skill.all.each do |skill|
			should have_css("input[type=\"checkbox\"][value=\"#{skill.id}\"]")
		end
	end

	describe "filling in correct info" do
		before do
			fill_in "Name", with: "Ruby developer"
			fill_in "Salary", with: "2500"
			fill_in "Contact info", with: "hr@ruby.com"
		end

		it "should create a new vacancy" do
			expect { click_button "Create Vacancy" }.to change(Vacancy, :count).by(1)
		end

		describe "elapsed date should be eq to 1 year 2 months 12 days" do
			before do
				page.select "In 1 year", from: "elapsed[years]"
				page.select "2 months", from: "elapsed[months]"
				page.select "12 days", from: "elapsed[days]"
				click_button "Create Vacancy"
			end

			specify { Vacancy.last.elapsed.should eq Hash["years"  => "1",
														  "months" => "2",
														  "days" => "12"] }
		end

		describe "added date should be {today}" do
			before { click_button "Create Vacancy" }
			 specify { Vacancy.last.added.should eq Date.today }
		end

		describe "adding a new skill via JavaScript", js: true do
			before do
				click_link "Add new"
				find(:css, "input[type=\"text\"][name=\"new_skill_0\"]").set("Prolog")
			end
			it "should create a new skill" do
				expect { click_button "Create Vacancy" }.to change(Skill, :count).by(1)
			end
		end

		describe "adding already existing skill via JavaScript", js: true do
			before do
				click_link "Add new"
				find(:css, "input[type=\"text\"][name=\"new_skill_0\"]").set("Ruby")
			end
			it "should not create a new skill" do
				expect { click_button "Create Vacancy" }.not_to change(Skill, :count)
			end
		end

		describe "selecting 3 skills" do
			before do
				skills = Skill.all.sample(3)
				skills.each do |skill|
					find(:css, "input[type=\"checkbox\"][value=\"#{skill.id}\"]").set(true)
				end
			end

			it "should create 3 relationships" do
				expect { click_button "Create Vacancy" }.to change(VacancySkillRelationship, :count).by(3)
			end
		end
	end

	describe "filling in wrong info" do
		before do
			fill_in "Salary", with: "dd12dd"
		end

		it "should not create a new vacancy" do
			expect { click_button "Create Vacancy" }.not_to change(Vacancy, :count).by(1)
		end

		describe "should generate error messages" do
			before { click_button "Create Vacancy" }

			it { should have_content "3 errors" }
			it { should have_content "Name can't be blank" }
			it { should have_content "Salary is not a number" }
			it { should have_content "Contact info can't be blank" }
		end
	end
end

