require 'spec_helper'

describe "Title Page" do
	before { visit root_path }
	subject { page }
	it { should have_selector('title', text: 'Employment Agency') }
	it { should have_selector('h1', text: 'Employment Agency') }

	describe "Employee links" do
		it { should have_link('Add', href: new_employee_path) }
		it { should have_link('List', href: employees_path) }
	end

	describe "Vacancy links" do
		it { should have_link('Add', href: new_vacancy_path) }
		it { should have_link('List', href: vacancies_path) }
	end
end