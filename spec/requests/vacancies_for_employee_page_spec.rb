#encoding: utf-8
require 'spec_helper'

describe "Vacancies For Employee" do
	include TestHelper
	include FactoryHelper

	let(:employee) { Employee.last }
	before do
		make_skills
		FactoryGirl.create(:employee, name: "Сара Мишель Геллар", skills: prepare_skills("Ruby", "HTML", "CSS"))
		FactoryGirl.create(:vacancy, name: "Ruby Junior developer", skills: prepare_skills("Ruby"))
		FactoryGirl.create(:vacancy, name: "Ruby Middle developer", skills: prepare_skills("Ruby", "HTML"))
		FactoryGirl.create(:vacancy, name: "Ruby Senior developer", skills: prepare_skills("Ruby", "HTML", "CSS", "XML"))
		visit vacancies_for_employee_path(employee)
	end

	subject { page }
	it { should have_selector('title', text: "Vacancies for employee: #{employee.name}") }
	it { should have_selector('h1', text: "Vacancies for employee: #{employee.name}") }

	it { should have_css("table#perfect_match_table tr"), count: 2 }
	it { should have_css("table#fuzzy_match_table tr"), count: 1 }

	it { find('#perfect_match_table').should have_content('Ruby Middle developer') }
	it { find('#perfect_match_table').should have_content('Ruby') }
	it { find('#perfect_match_table').should have_content('HTML') }

	it { find('#fuzzy_match_table').should have_content('Ruby Senior developer') }
	it { find('#fuzzy_match_table').should have_content('XML') }
	it { find('#fuzzy_match_table').should_not have_content('HTML') }
end















