require 'spec_helper'

describe "Vacancies list" do
	include TestHelper
	include FactoryHelper

	before do
		make_skills
		FactoryGirl.create(:vacancy, name: "Ruby Junior developer", skills: prepare_skills("Ruby"))
		FactoryGirl.create(:vacancy, name: "Ruby Middle developer", skills: prepare_skills("Ruby", "HTML"))
		FactoryGirl.create(:vacancy, name: "Ruby Senior developer", skills: prepare_skills("Ruby", "HTML", "CSS", "XML"))

		WillPaginate.per_page = 2
		visit vacancies_path
	end

	subject { page }
	it { should have_selector('h1', text: "Vacancies list") }
	it { should have_selector('title', text: "Vacancies list") }
	it { should have_selector('a', href: "vacancies?page=2", content: "Next")  }

end