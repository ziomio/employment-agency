module FactoryHelper
	def prepare_skills(*skills)
		results = []
		skills.each do |skill|
			results << Skill.find_by_name(skill)
		end
		results
	end
end

FactoryGirl::Proxy.send(:include, FactoryHelper)